﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;

namespace API_SECURITAS.Controllers
{
    public class CoberturasController : ApiController
    {
        private SEGURITASEntities1 DB = new SEGURITASEntities1();
        [HttpGet]
        public IEnumerable<Cobertura> Get()
        {
            using (SEGURITASEntities1 seguritasentities = new SEGURITASEntities1())
            {
                return seguritasentities.Coberturas.ToList();
            }
        }
        [HttpGet]
        public Cobertura Get(int id)
        {
            using (SEGURITASEntities1 seguritasentities = new SEGURITASEntities1())
            {
                return seguritasentities.Coberturas.FirstOrDefault(e => e.IDCob == id);
            }
        }
        [HttpPost]
        public IHttpActionResult AddCobertura([FromBody] Cobertura Cob)
        {
            if (ModelState.IsValid)
            {
                using (SEGURITASEntities1 DB = new SEGURITASEntities1())
                {
                    Cobertura oCob = new Cobertura
                    {
                        Descripcion = Cob.Descripcion,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Coberturas.Add(oCob);
                    DB.SaveChanges();
                    return Ok(oCob);
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public IHttpActionResult UpdCob(int id, [FromBody] Cobertura Cob)
        {
            if (ModelState.IsValid)
            {
                var CobExiste = DB.Coberturas.Count(c => c.IDCob == id) > 0;
                if (CobExiste)
                {
                    Cobertura oCob = new Cobertura
                    {
                        IDCob = Cob.IDCob,
                        Descripcion = Cob.Descripcion,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Entry(oCob).State = EntityState.Modified;
                    DB.SaveChanges();
                    return Ok(Cob);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        public IHttpActionResult EliminarCob(int id)
        {
            var Cob = DB.Coberturas.Find(id);
            if (Cob != null)
            {
                DB.Coberturas.Remove(Cob); DB.SaveChanges();
                return Ok(Cob);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
