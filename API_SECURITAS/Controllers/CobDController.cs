﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;
//using DBSEGURITAS;

namespace API_SECURITAS.Controllers
{
    public class CobDController : ApiController
    {
        [HttpGet]
        public IEnumerable<Cobertura> FNCbx()
        {
            List<Cobertura> oCob = new List<Cobertura>();
            DataLayer DL = new DataLayer();
            var oTT = DL.LLenaTabla("SP_CBX_Cobertura", "@ID", "1");
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oCob.Add(new Cobertura
                {
                    IDCob = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Descripcion = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oCob;
        }
        [HttpGet]
        public IEnumerable<Cobertura> FNCbxDISP(int IDPlan)
        {
            List<Cobertura> oCob = new List<Cobertura>();
            DataLayer DL = new DataLayer();
            var oTT = DL.LLenaTabla("SP_CBX_PlanCobDISP", "@IDPlan", IDPlan.ToString());
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oCob.Add(new Cobertura
                {
                    IDCob = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Descripcion = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oCob;
        }

    }
}
