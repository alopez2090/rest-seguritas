﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;

namespace API_SECURITAS.Controllers
{
    public class ClientesPlanController : ApiController
    {
        public IHttpActionResult AddClientePlan([FromBody] ClientePlane CliPla)
        {
            if (ModelState.IsValid)
            {
                using (SEGURITASEntities1 DB = new SEGURITASEntities1())
                {
                    ClientePlane oPlan = new ClientePlane
                    {
                        IDPlan = CliPla.IDPlan,
                        IDCliente = CliPla.IDCliente
                    };
                    DB.PlanCoberturas.Add(oPlan);
                    DB.SaveChanges();
                }
                return Ok(CliPla);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
