﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;
using DBSEGURITAS;

namespace API_SECURITAS.Controllers
{
    public class PlanDController : ApiController
    {
        private SEGURITASDATOSMODEL DB = new SEGURITASDATOSMODEL();
        [HttpGet]
        public IEnumerable<Planes> FNCbx()
        {
            List<Planes> oPlan= new List<Planes>();
            DataLayer DL = new DataLayer();
            var oTT = DL.LLenaTabla("SP_CBX_Planes", "@ID", "1");
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oPlan.Add(new Planes
                {
                    IDPlan = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Descripcion = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oPlan;
        }
    }
}
