﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;
//using DBSEGURITAS;
namespace API_SECURITAS.Controllers
{
    public class ClienteDController : ApiController
    {
        [HttpGet]
        public IEnumerable<Cliente> FNCbxCli()
        {
            List<Cliente> oCli = new List<Cliente>();
            DataLayer DL = new DataLayer();
            var oTT = DL.LLenaTabla("SP_CBX_Clientes", "@ID", "1");
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oCli.Add(new Cliente
                {
                    IDCli = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Nombre = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oCli;
        }
    }
}
