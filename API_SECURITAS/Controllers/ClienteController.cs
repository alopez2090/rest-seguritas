﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;

namespace API_SECURITAS.Controllers
{
    public class ClienteController : ApiController
    {
        
        private SEGURITASEntities1 DB = new SEGURITASEntities1();
        [HttpGet]
        public IEnumerable<Cliente> Get()
        {
            using (SEGURITASEntities1 usuariosentities = new SEGURITASEntities1())
            {
                return usuariosentities.Clientes.ToList();
            }
        }
        [HttpGet]
        public Cliente Get(int id)
        {
            using (SEGURITASEntities1 usuariosentities = new SEGURITASEntities1())
            {
                return usuariosentities.Clientes.FirstOrDefault(e => e.IDCli == id);
            }
        }
        [HttpPost]
        public IHttpActionResult AddCliente([FromBody] Cliente Cli)
        {
            if (ModelState.IsValid)
            {
                using (SEGURITASEntities1 DB = new SEGURITASEntities1())
                {
                    Cliente ocli = new Cliente
                    {
                        Nombre = Cli.Nombre,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Clientes.Add(ocli);
                    DB.SaveChanges();
                }
                return Ok(Cli);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public IHttpActionResult UpdCliente(int id, [FromBody] Cliente Cli)
        {
            
            if (ModelState.IsValid)
            {
                var ClienteExiste = DB.Clientes.Count(c => c.IDCli == id) > 0;
                if (ClienteExiste)
                {
                    Cliente ocli = new Cliente
                    {
                        IDCli = Cli.IDCli,
                        Nombre = Cli.Nombre,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Entry(ocli).State = EntityState.Modified;
                    DB.SaveChanges();
                    return Ok(ocli);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        public IHttpActionResult EliminarUsuario(int id)
        {
            var CliX = DB.Clientes.Find(id);
            if (CliX != null)
            {
                DB.Clientes.Remove(CliX); DB.SaveChanges();
                return Ok(CliX);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
