﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;
using API_SECURITAS;

namespace API_SECURITAS.Controllers
{
    public class ClientePlanCBController : ApiController
    {
        [HttpGet]
        public IEnumerable<PlanesX> FNCbxPLANDISP(int IDPlan)
        {
            List<PlanesX> oCob = new List<PlanesX>();
            DataLayer DL = new DataLayer(); 
            var oTT = DL.LLenaTabla("SP_CBX_ClientePlanesDISP", "@IDCli", IDPlan.ToString());            
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oCob.Add(new PlanesX
                {
                    IDPlan = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Descripcion = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oCob;
        }
    }
}
