﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;

namespace API_SECURITAS.Controllers
{
    public class PlanesController : ApiController
    {
        private SEGURITASEntities1 DB = new SEGURITASEntities1();
        [HttpGet]
        public IEnumerable<Plane> Get()
        {
            using (SEGURITASEntities1 seguritsentities = new SEGURITASEntities1())
            {
                return seguritsentities.Planes.ToList();
            }
        }
        [HttpGet]
        public Plane Get(int id)
        {
            using (SEGURITASEntities1 seguritsentities = new SEGURITASEntities1())
            {
                return seguritsentities.Planes.FirstOrDefault(e => e.IDPlan == id);
            }
        }
        [HttpPost]
        public IHttpActionResult AddPlan([FromBody] Plane Plan)
        {
            if (ModelState.IsValid)
            {
                using (SEGURITASEntities1 DB = new SEGURITASEntities1())
                {
                    Plane oPlan = new Plane
                    {
                        Descripcion = Plan.Descripcion,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Planes.Add(oPlan);
                    DB.SaveChanges();
                    return Ok(oPlan);
                }

            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPut]
        public IHttpActionResult UpdPlan(int id, [FromBody] Plane Plan)
        {
            if (ModelState.IsValid)
            {
                var PlanExiste = DB.Planes.Count(c => c.IDPlan == id) > 0;
                if (PlanExiste)
                {
                    Plane oPlan = new Plane
                    {
                        IDPlan = Plan.IDPlan,
                        Descripcion = Plan.Descripcion,
                        FechaModificacion = DateTime.Now
                    };
                    DB.Entry(oPlan).State = EntityState.Modified;
                    DB.SaveChanges();
                    return Ok(Plan);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        public IHttpActionResult EliminarPlan(int id)
        {
            var Plan = DB.Planes.Find(id);
            if (Plan != null)
            {
                DB.Planes.Remove(Plan); DB.SaveChanges();
                return Ok(Plan);
            }
            else
            {
                return NotFound();
            }
        }



    }
}
