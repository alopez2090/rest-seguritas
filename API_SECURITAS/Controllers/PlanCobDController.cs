﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_SECURITAS.Models;
using DBSEGURITAS;

namespace API_SECURITAS.Controllers
{
    public class PlanCobDController : ApiController
    {
        private SEGURITASDATOSMODEL DB = new SEGURITASDATOSMODEL();
        [HttpGet]
        public IEnumerable<PlanCoberturas> Get(int nIDPlanCon)
        {
            List<PlanCoberturas> oPlanCob = new List<PlanCoberturas>();
            DataLayer DL = new DataLayer();
            var oTT = DL.LLenaTabla("SP_LLENA_TABLA_PlanCoberturas", "@IDPlan", nIDPlanCon.ToString());
            for (int K = 0; K < oTT.Tables[0].Rows.Count; K++)
            {
                oPlanCob.Add(new PlanCoberturas
                {
                    IDPlanCob = int.Parse(oTT.Tables[0].Rows[K].ItemArray[0].ToString()),
                    Descripcion = oTT.Tables[0].Rows[K].ItemArray[1].ToString(),
                });
            }
            oTT.Dispose(); oTT = null;
            DL = null;
            return oPlanCob;
        }
        [HttpPost]
        public IHttpActionResult AddPlanCob([FromBody] PlanCobertura PlanCob)
        {
            if (ModelState.IsValid)
            {
                using (SEGURITASEntities1 DB = new SEGURITASEntities1())
                {
                    PlanCobertura oPlan = new PlanCobertura
                    {
                        IDPlan = PlanCob.IDPlan,
                        IDCob = PlanCob.IDCob
                    };
                    DB.PlanCoberturas.Add(oPlan); DB.SaveChanges();                    
                }
                return Ok(PlanCob);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        public IHttpActionResult EliminarPlanCob(int id)
        {
            SEGURITASEntities1 DBX = new SEGURITASEntities1();
            if (ModelState.IsValid)
            {
                var PlanCob = DBX.PlanCoberturas.Count(c => c.IDPlanCob == id) > 0;
                if (PlanCob)
                {
                    var oPlanCob = DBX.PlanCoberturas.Find(id);
                    DBX.PlanCoberturas.Remove(oPlanCob); DBX.SaveChanges();
                    return Ok(PlanCob);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
