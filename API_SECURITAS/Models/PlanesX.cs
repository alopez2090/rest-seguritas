﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_SECURITAS.Models
{
    public class PlanesX
    {
        public int IDPlan { get; set; }
        public string Descripcion { get; set; }
        public int IDCob { get; set; }
        public string FechaModificacion { get; set; }
        public IEnumerable<PlanesX> ICbxPlanes { get; set; }
        public IEnumerable<PlanesX> IPlanes { get; set; }
    }
}