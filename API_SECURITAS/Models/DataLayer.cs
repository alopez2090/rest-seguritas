﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace API_SECURITAS.Models
{
    public class DataLayer
    {
        public SqlConnection DB = null; public SqlCommand cmd = null;
        public SqlCommand FNCMD(string SQL)
        {
            string WDB = ConfigurationManager.ConnectionStrings["SEGURITASDATOSMODEL"].ConnectionString;
            DB = new SqlConnection(WDB); DB.Open(); cmd = DB.CreateCommand(); cmd.CommandTimeout = 0 ; cmd.CommandText = SQL;
            return cmd;
        }

        public SqlDataReader SPRdrSPX(string WSP, int nData, string WPar, params string[] oVal)
        {
            char CLim = '|'; cmd = FNCMD(WSP); cmd.CommandType = CommandType.StoredProcedure; string[] oPars = WPar.Split(CLim);
            for (int K = 0; K <= oVal.Length - 1; K++) cmd.Parameters.AddWithValue(oPars[K], oVal[K]);
            Array.Clear(oPars, 0, oPars.Length); oPars = null; Array.Clear(oVal, 0, oVal.Length); oVal = null;
            if (nData == 1)
            {
                cmd.ExecuteNonQuery(); return null;
            }
            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataAdapter SPRdrSP(string WSP, string WPar, params string[] oVal)
        {
            char CLim = '|'; cmd = FNCMD(WSP); cmd.CommandType = CommandType.StoredProcedure; string[] oPars = WPar.Split(CLim);
            for (int K = 0; K <= oVal.Length - 1; K++) cmd.Parameters.AddWithValue(oPars[K], oVal[K]);
            Array.Clear(oPars, 0, oPars.Length); oPars = null; Array.Clear(oVal, 0, oVal.Length); oVal = null;
            var Adapter = new SqlDataAdapter(cmd);
            return Adapter;
        }

        public DataSet LLenaTabla(string WSP, string WPar, params string[] oVal)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter oADPT = SPRdrSP(WSP, WPar, oVal);
            oADPT.Fill(ds); oADPT.Dispose(); oADPT = null;
            return ds;
        }


        public DataSet LLenaTablaX(string SQL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = FNCMD(SQL);
            adapter.Fill(ds);
            return ds;
        }
    }
}