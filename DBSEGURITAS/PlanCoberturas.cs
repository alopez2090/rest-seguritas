﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace DBSEGURITAS
{

    public partial class PlanCoberturas
    {
        [Key]
        public int IDPlanCob { get; set; }
        [StringLength(250)]
        public string Descripcion { get; set; }        
    }

}
