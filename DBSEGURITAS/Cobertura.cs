namespace DBSEGURITAS
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cobertura
    {
        [Key]
        public int IDCob { get; set; }

        [StringLength(250)]
        public string Descripcion { get; set; }

        public DateTime? FechaModificacion { get; set; }
    }
}
