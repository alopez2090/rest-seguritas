namespace DBSEGURITAS
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SEGURITASDATOSMODEL : DbContext
    {
        public SEGURITASDATOSMODEL()
            : base("name=SEGURITASDATOSMODEL")
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Cobertura> Coberturas { get; set; }
        public virtual DbSet<Planes> Planes { get; set; }
        public virtual DbSet<PlanCoberturas> PlanCoberturas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Cobertura>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Planes>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<PlanCoberturas>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);
        }
    }
}
