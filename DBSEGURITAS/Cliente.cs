namespace DBSEGURITAS
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cliente
    {
        [Key]
        public int IDCli { get; set; }

        [StringLength(250)]
        public string Nombre { get; set; }

        public DateTime? FechaModificacion { get; set; }
    }
}
